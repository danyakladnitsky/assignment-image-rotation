#include "file.h"

int close_file(FILE *file){
  
  if(fclose(file) != 0){
    return READ_ERROR;
  }

  return 0;
}

enum write_operations save_image(char *const image_name, struct image *const image_file) {
  FILE * file;
  // open file and save
  file = fopen(image_name, "wb");

  if (file == NULL) {
    return WRITE_ERROR;
  }

  enum write_operations write_bmp_a = encode_bmp(file, image_file);

  if (write_bmp_a != WRITE_SUCCESS) {
    return write_bmp_a;
  }

  if (fclose(file) != 0) {
    return WRITE_ERROR;
  }

  return WRITE_SUCCESS;
}

enum read_operations get_image(char *const image_name, struct image *const image_file) {
  FILE * file;
  // open file and read
  file = fopen(image_name, "rb");

  if (file == NULL) {
    return READ_ERROR;
  }

  enum read_operations read_bmp_a = decode_bmp(file, image_file);

  int file_close_status = close_file(file);

  if(file_close_status == READ_ERROR){
    return file_close_status;
  }

  if (read_bmp_a != READ_SUCCESS) {
    return read_bmp_a;
  }


  return READ_SUCCESS;
}


