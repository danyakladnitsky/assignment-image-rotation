#include "image.h"

#include "bmp.h"

// defining useful constants
#define COMPRESSION 0
#define X_PER_METER 0
#define Y_PER_METER 0
#define IMAGE_TYPE 19778
#define BI_PLANES 1
#define BI_SIZE 40
#define IMPORTANT_COLORS 0
#define NUM_COLORS 0
#define HEADER_SIZE 54
#define BIT_COUNT 24

static enum read_operations handle_read(struct pixel * pixels, size_t w, size_t h, uint32_t padding_value, FILE * in ) {
    for (size_t i = 0; i < h; i++) {
        if (fread(pixels + w * i, sizeof(struct pixel), w, in ) != w) {
            free(pixels);
            return READ_ERROR;
        }
        if (fseek( in , padding_value, SEEK_CUR) != 0) {
            free(pixels);
            return READ_ERROR;
        }
    }

    return READ_SUCCESS;
}

static uint32_t calculate_padding(uint32_t width) {
    // calculate padding
    const uint8_t value = (width * sizeof(struct pixel)) % 4;
    const uint8_t padding = value;

    if (padding) {
        return 4 - padding;
    }

    return 0;
}

static enum read_operations read_data(FILE * file,
    const struct image img) {
    struct pixel * target_pixels = img.data;

    // calculate padding value
    uint32_t padding_value = calculate_padding(img.width);

    size_t w = img.width;
    const size_t h = img.height;

    return handle_read(target_pixels, w, h, padding_value, file);
}

static enum write_operations fill_image_data(const size_t height,
    const size_t width, struct pixel * current_pixel, uint32_t padding, FILE * file) {
    for (size_t i = 0; i < height; i++) {
        if (fwrite(current_pixel, sizeof(struct pixel), width, file) != width)
            return WRITE_ERROR;
        if (fwrite(current_pixel, 1, padding, file) != padding)
            return WRITE_ERROR;
        current_pixel = current_pixel + (size_t) width;
    }

    return WRITE_SUCCESS;
}
static enum write_operations save_image_content(FILE * file, struct image const img) {
    // filling image data
    uint32_t padding = calculate_padding(img.width);

    // filling image struct
    const size_t width = img.width;
    const size_t height = img.height;

    struct pixel * current_pixel = img.data;

    return fill_image_data(height, width, current_pixel, padding, file);
}

static enum read_operations read_image_header(FILE * file, struct bmp_header * header_info) {
    size_t header_size = sizeof(struct bmp_header);
    if (fread(header_info, header_size, 1, file) == 1) {
        return READ_SUCCESS;
    }

    return READ_ERROR;
}

static struct bmp_header create_header(struct image const * image_data){
    struct bmp_header header = {
        0
    };
    
    uint32_t padding = calculate_padding(image_data -> width);

    size_t image_size = sizeof(struct pixel) * image_data -> width * image_data -> height + padding * image_data -> height;

    header.biSizeImage = image_size;
    header.bFileSize = HEADER_SIZE + image_size;

    header.bOffBits = HEADER_SIZE;
    header.biSize = BI_SIZE;

    header.biCompression = COMPRESSION;

    header.biXPelsPerMeter = X_PER_METER;
    header.biYPelsPerMeter = Y_PER_METER;

    header.biWidth = image_data -> width;
    header.biHeight = image_data -> height;

    header.biPlanes = BI_PLANES;
    header.biBitCount = BIT_COUNT;

    header.biClrUsed = NUM_COLORS;
    header.biClrImportant = IMPORTANT_COLORS;

    header.bfType = IMAGE_TYPE;

    return header;
}

static enum write_operations save_header(FILE * out, struct image
    const * image_data) {
    struct bmp_header header = create_header(image_data);

    if (fwrite( & header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    return WRITE_SUCCESS;
}

enum write_operations encode_bmp(FILE * file, struct image
    const * img) {
    // handle errors
    if (save_header(file, img) != WRITE_SUCCESS) {
        return WRITE_ERROR;
    }

    return save_image_content(file, * img);
}

enum read_operations decode_bmp(FILE * file, struct image * img) {
    struct bmp_header header = {
        0
    };
    enum read_operations header_check = read_image_header(file, & header);
    if (header_check != READ_SUCCESS) {
        return header_check;
    }

    * img = create_img(header.biWidth, header.biHeight);

    enum read_operations read_check = read_data(file, * img);

    return read_check;
}
