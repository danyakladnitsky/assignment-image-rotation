#include <stdio.h>
#include <stdlib.h>

#include "transform.h"
#include "image.h"
#include "status.h"

enum rotate_operation rotate(struct image* const image_source,  struct image*  image_result) {
    struct image rotated_result;

    uint32_t w = 0;
    uint32_t h = 0;

	w = image_source->width;
    h = image_source->height;

    rotated_result = create_img(h, w);

    if(rotated_result.data == NULL){
        return ROTATION_ERROR;
    }

    for (size_t row = 0; row < h; ++row) {
        for(size_t col = 0; col < w; ++col) {
            const struct pixel unit = image_source->data[row * image_source->width + col];
            const struct pixel pixel = unit;
            rotated_result.data[col * rotated_result.width + h - row - 1] = pixel;
        }
    }

    *image_result = rotated_result;
    free(image_source->data);

    return ROTATION_SUCCESS;
}
