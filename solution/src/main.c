#include "bmp.h"
#include "file.h"
#include "image.h"
#include "transform.h"
#include <stdio.h>

int main(int argc, char ** argv) {
    if (argc < 3) {
        fprintf(stderr, "%s", "Enter input / output images\n");
        return 0;
    }

    char *const input_image_path = argv[1];
    char *const output_image_path = argv[2];
    
    printf("Input image path: %s \n", input_image_path);

    printf("Output image path: %s \n", output_image_path);

    if(argc > 3){
        fprintf(stderr, "%s", "Omitting extra arguments \n");
    }
    
    fprintf(stdout, "%s", "Transforming images...\n");


    struct image source_image = {
        0
    };

    struct image result_image = {
        0
    };

    enum read_operations read_status = get_image(input_image_path, &source_image);
    if (read_status == 1) {
        return 0;
    }

    enum rotate_operation rotation_status = rotate(&source_image, &result_image);

    if(rotation_status == ROTATION_ERROR){
        printf("Error occured. Image rotation failed\n");
        exit(1);
    }

    enum write_operations get_status = save_image(output_image_path, &result_image);
    free(result_image.data);

    if (get_status == 1) {
        return 0;
    }

    return 0;
}
