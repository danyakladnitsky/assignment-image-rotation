#include "image.h"

struct image create_img(size_t w, size_t h) {
  struct image image_entity = {
    0
  };

  // filling struct
  image_entity.width = w;
  image_entity.height = h;

  size_t size = w * h * sizeof(struct pixel);

  // obtain memory
  image_entity.data = malloc(size);
  return image_entity;
}
