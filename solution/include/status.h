#ifndef STATUS
#define STATUS

enum read_operations {
	READ_SUCCESS = 0,
	READ_ERROR = 1
};

enum  write_operations {
	WRITE_SUCCESS = 0,
	WRITE_ERROR = 1
};

enum  rotate_operation {
	ROTATION_SUCCESS = 0,
	ROTATION_ERROR = 1
};


#endif

