#ifndef FILE_OPS
#define FILE_OPS
#include <stdio.h>
#include "image.h"
#include "status.h"
#include "bmp.h"

enum read_operations get_image(char* const image_name, struct image* const image);

enum write_operations save_image(char* const image_name, struct image* const image);

#endif
