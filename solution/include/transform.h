#include  <stdint.h>
#include "image.h"
#include "status.h"

enum rotate_operation rotate(struct image* const image_source, struct image*  image_result);
